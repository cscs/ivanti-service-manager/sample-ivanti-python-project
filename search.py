import ivantiservicemanager as ism
import json

# Open general settings
with open('settings.json', 'r') as f:
    settings = json.load(f)

# Ivanti CI objects to register against client
models = [
    {
        "Name": "ci_infrastructure_device",
        "BusinessObject": "CI#Switch"
    },
]

# Connect to API
api_client = ism.Api(tenant_id=settings['tenantId'], url=settings['heatUrl'])
api_client.load_models(models)

api_client.connect(settings['username'], settings['password'], settings['role'])

# Get all CIs with limit

# You can set the limit higher as needed. There isn't paging yet so if you know there are
# around 500 devices use limit=600 or something. Note if you don't specify a limit it
# will default to 5.
cis = api_client.ci_infrastructure_device.get_all(limit=5)
print("\n\n************************************************************")
print("Example 1 - Get All with limit:")
for ci in cis:
    print(ci.Name)

# Get by field and also discover what fields you can access

# You can search by any field using a passed in dict
# i.e.
# {'Name':'bob', 'PhysicalName':'dogs'}
# You can use multiple fields. But all of them apply is an AND clause at the moment. It'll be expanded later to include
# OR
# You can use the vars() command to see which fields come back from ivanti for using in get_by

cis = api_client.ci_infrastructure_device.get_by({'Name': 'sw-agg04'})

print("\n\n************************************************************")
print("Example 2 - Get by field default:")
for ci in cis:
    print("{0} [{1}]".format(ci.Name, ci.AA_DeviceType))
    print(vars(ci))


# Get by field with wildcard

# You can use % as a wildcard in get_by searches
cis = api_client.ci_infrastructure_device.get_by({'Name': 'sw-agg%'})
print("\n\n************************************************************")
print("Example 3 - Get by field with a wildcard:")
for ci in cis:
    print("{0} [{1}]".format(ci.Name, ci.AA_DeviceType))

# Non-existent item

print("\n\n************************************************************")
print("Example 4 - Handling missing item exceptions")

# If an ivanti item isn't found you can check for a specific exception
try:
    cis = api_client.ci_infrastructure_device.get_by({'Name': 'stupid_switch'})
    print("Get by field default:")
    for ci in cis:
        print(ci.Name)
except ism.BuinessObjectNotFoundError as e:
    if "NotFound" in e.args[0]:
        print("Ivanti item not found.")
    else:
        # some other error, re-raise
        raise
