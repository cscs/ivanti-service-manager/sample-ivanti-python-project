# Sample Ivanti Service Manager Tool

This project provides a starting point for creating your very
own Ivanti python project.

## Setup

There are only really two steps you need to do. Setup libraries and setup config.

Once you've copied this repository you should delete the existing .git directory
and create a new git repo. The included .gitignore should be kept as it
will help exclude files you don't need.

### Setup config

Copy settings.json.template to settings.json and fill in the appropriate fields.
If you need the testing details ask the Service Desk or Dev teams.

### Install requirements

You should setup a new virtual environment for this project.
You may need to adapt the source activation command 
for mac/linux/windows. Search for it if you need to.

```
python -m venv venv
source  venv/bin/activate
pip install -r requirements.txt
```

This will get you to a ready to run the test script!

### Test

`python search.py`

This should return a few CI sample details which should be enough
to get you started.

